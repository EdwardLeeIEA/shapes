
package com.libertymutual.student.Edward.programs.example01;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.libertymutual.student.Edward.programs.example01.shapes.Circle;
import com.libertymutual.student.Edward.programs.example01.shapes.Square;
import com.libertymutual.student.Edward.programs.example01.shapes.Rectangle;
import java.awt.Color;
//import java.math.BigDecimal;

public class Application {

	private final static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {

		logger.info(Application.class + " starting up now ");

        int radius = 10;
        Circle circle = new Circle(radius, Color.PINK);
        System.out.println(circle.getArea());

        int length = 100;
        Square square = new Square(length, Color.RED);
        System.out.println(square.getArea());

		int width = 50;
        length = 100;
        Rectangle rectangle = new Rectangle(width, length, Color.RED);
        System.out.println(rectangle.getArea());
    }

		//System.exit(0);
}



