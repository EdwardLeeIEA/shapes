
package com.libertymutual.student.Edward.programs.example01.shapes;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Color;
import java.math.BigDecimal;

public class Circle extends Shape {

	private final static Logger logger = LogManager.getLogger(Circle.class);

    private int radius;

    public Circle(int radius, Color color) {
        super(color);
        this.radius = radius;
		logger.info(" Circle starting ");
    }

    // provide a getArea implementation 



    @Override
    public BigDecimal getArea() {
        double pi = 3.14;
        double area = pi * radius * radius;
        return new BigDecimal(area);
    }
}
