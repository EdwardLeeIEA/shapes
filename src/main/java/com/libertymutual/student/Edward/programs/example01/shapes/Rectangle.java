
package com.libertymutual.student.Edward.programs.example01.shapes;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.Color;
import java.math.BigDecimal;



public class Rectangle extends Shape {

	//private final static Logger logger = LogManager.getLogger(Rectangle.class);

    private int length;
	private int width;

	//logger.info(Rectangle.class + " starting ");

    public Rectangle(int length, int width, Color color) {
		
        super(color);
        this.length = length;
		this.width = width;
    }

    // provide a getArea implementation 

    @Override
    public BigDecimal getArea() {
        double area = length * width;
        return new BigDecimal(area);
    }
}
