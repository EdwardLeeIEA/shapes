
package com.libertymutual.student.Edward.programs.example01.shapes;

import org.junit.Test;

import java.awt.Color;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class SquareTest {

    @Test
    public void testGetArea() {
        Square square = new Square(100, Color.red);
        BigDecimal area = square.getArea();
        BigDecimal expectedAnswer = new BigDecimal(10000);
        assertEquals("Verify that the area is correct", expectedAnswer, area);
    }

}
