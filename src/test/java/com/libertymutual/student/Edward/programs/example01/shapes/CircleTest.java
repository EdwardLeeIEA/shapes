
package com.libertymutual.student.Edward.programs.example01.shapes;
import static org.junit.Assert.assertEquals;
import java.awt.Color;
import java.math.BigDecimal;
import org.junit.Test;

public class CircleTest {

    @Test
    public void testGetArea() {
        Circle circle = new Circle(10, Color.red);
        BigDecimal area = circle.getArea();
        BigDecimal expectedAnswer = new BigDecimal(314);
        assertEquals("Verify that the area is correct", expectedAnswer, area);
    }

}
